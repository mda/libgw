/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.

   @section DESCRIPTION
   
   This header file provides  the interface to the algorithm
   for  the   analytical  closed-form  computation   of  the
   guesswork of any given qubit ensemble.
*/

#ifndef GW
#define GW

#include "sym.h"

typedef struct gw gw;

/**
   @brief Construct a state for the guesswork-computing algorithm.
   @param s are the symmetries of the ensemble.
   @return the state of the algorithm.
 */
gw* gw_new(sym* s);

/**
   @brief Destroy a state of the guesswork-computing algorithm.
   @param g is the state to be destroyed.
 */
void gw_destroy(gw* g);

/**
   @brief Initializes a state of the guesswork-computing algorithm.
   @param g is the state to be initialized.
   @param s are the symmetries of the ensemble. 
 */
void gw_init(gw* g, sym* s);

/**
   @brief Free a state of the guesswork-computing algorithm.
   @param g is the state to be freed.
 */
void gw_free(gw* g);

/**
   @brief Executes the guesswork-computing algorithm.
   @param g is the state of the algorithm.
 */
void gw_proc(gw* g);

/**
   @brief Get the guesswork of the ensemble.  
   @param g is the state of the algorithm.
   @return the guesswork of the ensemble.
*/
scal* gw_get(gw* g);

#endif
