/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.

   @section DESCRIPTION
   
   This   header  file   provides  the   interface  to   the
   polynomial-time      division-free     symmetries-finding
   algorithm  for  any given  arbitrary-dimensional  complex
   point set.
*/

#ifndef SYM
#define SYM

#include "array.h"
#include "matrix.h"
#include "node.h"

compar vect_cmp;

typedef struct sym sym;

/**
   @brief Construct a new state for the symmetries-finding algorithm.
   @param an is the tuple of vectors.
   @return the state of the algorithm.
 */
sym* sym_new(array* an);

/**
   @brief Destroy a state for the symmetries-finding algorithm.
   @param s is the state to be destroyed.
 */
void sym_destroy(sym* s);

/**
   @brief Initialize a state for the symmetries-finding algorithm.
   @param s is the state to be initialized.
   @param an is the tuple of vectors.
 */
void sym_init(sym* s, array* an);

/**
   @brief Free a state for the symmetries-finding algorithm.
   @param s is the state to be freed.
 */
void sym_free(sym* s);

/**
   @brief Run the symmetries-finding algorithm.
   @param s is the state of the algorithm.
   @param an is the tuple of vectors.
 */
void sym_proc(sym* s, array* an);

/**
   @brief Get the list of symmetries from the state of the algorithm.
   @param s is the state of the algorithm.
   @return the list of symmetries.
 */
node* sym_getnode(sym* s);

/**
   @brief Get the tuple of vectors from the state of the algorithm.
   @param s is the state of the algorithm.
   @return the tuple of vectors.
 */
array* sym_getarray(sym* s);

/**
   @brief Get the number of symmetries from the state of the algorithm.
   @param s is the state of the algorithm.
   @return the numer of symmetries.
 */
unsigned sym_getnum(sym* s);

/**
   @brief Compare two vectors according to their components along a basis.
   @param v0void is the first operand.
   @param v1void is the second operand.
   @param avoid tuple of basis vectors. 
   @return @f$-1@f$ if @f$v_0 < v_1@f$, @f$1@f$ if @f$v_0 > v_1@f$, @f$0@f$ otherwise. 
 */
int vect_cmp(void const* v0void, void const* v1void, void* avoid);

#endif
