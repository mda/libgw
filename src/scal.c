/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

#include "scal.h"

scal* scal_new(ring* r) {
    scal* x = malloc(sizeof(scal)); // TODO
    r->init(x, r);
    return x;
}

void scal_destroy(scal* x) {
    x->r->free(x);
    free(x);
}

void scal_set(scal* x, char* s) {
    //printf("A\n");
    assert(x != 0);
    x->r->set(x, s);
    //printf("B\n");
}

char* scal_get(scal const* x, char* s) {
    return x->r->get(x, s);
}

void scal_copy(scal* dest, scal const* src) {
    assert(dest->r == src->r);
    dest->r->copy(dest, src);
}

void scal_swap(scal* x, scal* y) {
    assert(x->r == y->r);
    void* v = x->v;
    x->v = y->v;
    y->v = v;
}

void scal_add(scal* res, scal const* op0, scal const* op1) {
    assert((res->r == op0->r) && (res->r == op1->r));
    res->r->add(res, op0, op1);
}

void scal_sub(scal* res, scal const* op0, scal const* op1) {
    assert((res->r == op0->r) && (res->r == op1->r));
    res->r->sub(res, op0, op1);
}

void scal_mul(scal* res, scal const* op0, scal const* op1) {
    assert((res->r == op0->r) && (res->r == op1->r));
    res->r->mul(res, op0, op1);
}

void scal_mul_i(scal* res, int op0, scal const* op1) {
    assert(res->r == op1->r);
    res->r->mul_i(res, op0, op1);
}

void scal_addmul(scal* res, scal const* op0, scal const* op1) {
    assert((res->r == op0->r) && (res->r == op1->r));
    res->r->addmul(res, op0, op1);
}

void scal_addmul_i(scal* res, int op0, scal const* op1) {
    assert(res->r == op1->r);
    res->r->addmul_i(res, op0, op1);
}

int scal_comp(scal const* op0, scal const* op1) {
    assert(op0->r == op1->r);
    return op0->r->comp(op0, op1);
}

void scal_max(scal* res, scal const* op0, scal const* op1) {
    assert((res->r == op0->r) && (res->r == op1->r));
    scal_copy(res, (scal_comp(op0, op1) >= 0) ? op0 : op1);
}
