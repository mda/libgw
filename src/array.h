/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.

   @section DESCRIPTION
   
   This  header file  provides  the  interface to  functions
   manipulating tuples of pointers to arbitrary objects.
*/

#ifndef ARRAY
#define ARRAY

typedef struct array array;

/**
   @struct indexes
   @brief Ordered pair of indexes.
*/
typedef struct indexes {
    unsigned j; /**< First index of the pair. */
    unsigned k; /**< Second index of the pair. */
} indexes;

typedef int compar(void const* elem0, void const* elem1, void*);

typedef void destroyvoid(void* p);

/**
   @brief Construct a new tuple.
   @param num is the number of elements.
   @return the new tuple.
 */
array* array_new(unsigned num);

/**
   @brief Destroy a tuple.
   @param a is the tuple to be destroyed.
   @param f is the destructor function to be called for the elements of the tuple.
 */
void array_destroy(array* a, destroyvoid f);

/**
   @brief Initialize a tuple.
   @param a is the tuple to be initialized.
   @param num is the number of elements.
 */
void array_init(array* a, unsigned num);

/**
   @brief Free a tuple.
   @param a is the tuple to be freed.
   @param f is the destructor function to be called for the elements of the tuple.
 */
void array_free(array* a, destroyvoid* f);

/**
   @brief Copy a tuple.
   @param dst is the tuple to be copied to.
   @param src is the tuple to be copied from.
 */
void array_cpy(array* dst, array* src);

/**
   @brief Shrink a tuple to a subtuple.
   @param first is the first element of the shrinked tuple.
   @param len is the number of elements of the shrinked tuple.
 */
void array_sub(array* a, unsigned first, unsigned len);

/**
   @brief Get the number of element of a tuple.
   @param a is the tuple.
   @return the number of elements.
 */
unsigned array_getnum(array* a);

/**
   @brief Get an element of a tuple.
   @param a is the tuple.
   @param i is the element to be retrieved.
   @return the @f$i@f$-th element of the tuple.
 */
void* array_get(array* a, unsigned i);

/**
   @brief Set an element of a tuple.
   @param a is the tuple.
   @param i is the element to be set.
   @param is the value to be assigned to the  @f$i@f$-th element.
 */
void array_set(array* a, unsigned i, void* p);

/**
   @brief Get the id of an element of a tuple.
   @param a is the tuple.
   @param i is the element to be set.
   @param the id of the @f$i@f$-th element.
 */
unsigned array_getid(array* a, unsigned i);

/**
   @brief Swap two values of a tuple.
   @param a is the tuple.
   @param i is the pair of indexes to be swapped.
 */
indexes array_swap(array* a, indexes i);

/**
   @brief Sort the elements of a tuple.
   @param a is the tuple to be sorted.
   @param f is the ordering according to which the sorting is performed.
   @param p is data passed to the ordering f.
*/
void array_sort(array* a, compar* f, void* p);

typedef void array_perm_proc(array* a, indexes i, void *p);
typedef void array_comb_proc(array* an, array* at, indexes i, void* p);
typedef void array_tuple_proc(array* a, unsigned i, void *);

/**
   @brief Generate all the permutations of a tuple
   @param a is the tuple to be permuted.
   @param f is the function to be called for each permutation.
   @param p is data passed to function f.
*/
void array_perm(array* a, array_perm_proc f, void* p);

/**
   @brief Generate all the combinations of t elements of a tuple
   @param a is the tuple.
   @param t is the number of elements in each combination.
   @param f is the function to be called for each combination.
   @param p is data passed to function f.
*/
void array_comb(array* a, unsigned t, array_comb_proc f, void* p);

/**
   @brief Generate all the binary tuples of a tuple.
   @param a is the tuple.
   @param visit is the function to be called for each binary tuple.
   @param p is data passed to function visit.
*/
void array_tuple(array* a, array_tuple_proc visit, void* p);

/**
   @brief Relabel the elements of the tuple in the natural order..
   @param a is the tuple.
*/
void array_reset(array* a);

#endif
