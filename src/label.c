/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

/**
   @struct label
   @brief Label of each element of a tuple.
*/
typedef struct label {
    unsigned id; /**< @brief Element's id. */
    void* value; /**< @brief Element's value. */
} label;

label* label_new(unsigned id, void* value) {
    label* l = malloc(sizeof(label));
    l->id = id;
    l->value = value;
    return l;
}

void label_destroy(label* l) {
    free(l);
}

unsigned label_getid(label const* l) {
    return l->id;
}

void label_setid(label* l, unsigned i) {
    l->id = i;
}

void* label_getvalue(label const* l) {
    return l->value;
}

void label_setvalue(label* l, void* v) {
    l->value = v;
}

void label_cpy(label* dst, label const* src) {
    label_setid(dst, label_getid(src));
    label_setvalue(dst, label_getvalue(src));
}
