/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include<stdlib.h>

#include "sym.h"
#include "vect.h"

typedef void sym_branch(array* an, array* ad, sym* s);

/**
   \struct sym
   \brief State of the symmetries-finding algorithm.
*/
typedef struct sym {
    array* an; /**< Tuple of Pauli vectors */
    unsigned n, d, count;
    ring* r;
    sym_branch *f;
    matrix* md;
    matrix* mn;
    node* last;
} sym;

static void sym_stage0(array* an, array* ad, indexes i, void* p);
static void sym_stage1(array* ad, indexes i, void *p);

static void sym_branch0(array* an, array* ad, sym* s);
static void sym_branch1(array* an, array* ad, sym* s);

sym* sym_new(array* an) {
    sym* s = malloc(sizeof(sym)); // TODO
    sym_init(s, an);
    return s;
}

void sym_destroy(sym* s) {
    sym_free(s);
    free(s);
}

void sym_init(sym* s, array* an) {
    s->n = array_getnum(an);
    s->d = vect_getdim((vect*) array_get(an, 0));
    s->count = 0;
    s->r = vect_getring((vect*) array_get(an, 0));
    s->f = sym_branch0;
    s->md = matrix_new(s->r, s->d, s->d);
    s->mn = matrix_new(s->r, s->n, s->n);
    s->an = array_new(s->n);
    array_cpy(s->an, an);
    s->last = 0;
}

void sym_free(sym* s) {
    matrix_destroy(s->mn);
    matrix_destroy(s->md);
    array_destroy(s->an, 0);
}

void sym_proc(sym* s, array* an) {
    array_comb(an, 3, sym_stage0, s);
}

// array_comb_proc
static void sym_stage0(array* an, array* ad, indexes i, void* p) {
    sym* s = (sym*) p;
    s->f(an, ad, s);
}

static void sym_branch0(array* an, array* ad, sym* s) {
    matrix_gram(s->md, ad);

    scal* det = scal_new(s->r);
    matrix_det(det, s->md);
    scal* z = scal_new(s->r); // Defaults to zero
    if (scal_comp(det, z)) {
	array_sort(s->an, vect_cmp, ad);
	array_reset(s->an);
	matrix_gram(s->mn, s->an);
	s->f = sym_branch1;
	sym_branch1(an, ad, s);
    }
    scal_destroy(z);
    scal_destroy(det);
}

static void sym_branch1(array* an, array* ad, sym* s) {
    array_perm(ad, sym_stage1, s);
}

// array_perm_proc
static void sym_stage1(array* ad, indexes i, void *p){
    sym* s = p;
    matrix* md = matrix_new(s->r, s->d, s->d);
    matrix_gram(md, ad);
    if (!matrix_eq(s->md, md)) return;

    array* an = array_new(s->n);
    array_cpy(an, s->an);
    array_sort(an, vect_cmp, ad); // n log(n)

    matrix* mn =  matrix_new(s->r, s->n, s->n);
    matrix_gram(mn, an); // n^2

    if (matrix_eq(s->mn, mn)) {
	s->count++; // n^2
	node* nd = node_new(an);
	node_cat(s->last, nd);
	s->last = nd;
    } else {
	array_destroy(an, 0);
    }
    matrix_destroy(mn);
}

int vect_cmp(void const* v0void, void const* v1void, void* avoid) {
    vect const* v0 = v0void;
    vect const* v1 = v1void;
    array* a = avoid;
    int sgn;
    ring* r = vect_getring(v0);
    scal* x0 = scal_new(r);
    scal* x1 = scal_new(r);
    
    for (unsigned i = 0; i < array_getnum(a); ++i) {
	vect_inprod(x0, (vect*) array_get(a, i), v0);
	vect_inprod(x1, (vect*) array_get(a, i), v1);
	if ((sgn = scal_comp(x0, x1)) || i == array_getnum(a) - 1) {
	    scal_destroy(x0);
	    scal_destroy(x1);
	    return -sgn;
	}
    }
    printf("Error: ordering is not defined\n");
    return 0;
}

node* sym_getnode(sym* s) {
    return s->last;
}

array* sym_getarray(sym* s) {
    return s->an;
}

unsigned sym_getnum(sym* s) {
    return s->count;
}
