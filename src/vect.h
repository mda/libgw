/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.

   @section DESCRIPTION
   
   This  header file  provides  the  interface to  functions
   manipulating vectors over an arbitrary given ring.
*/

#ifndef VECT
#define VECT

#include "ring.h"
#include "scal.h"

typedef struct vect vect;

/**
   @brief Construct a new vector.
   @param r is the ring of the matrix elements.
   @param s is the string representation of the vector.
   @return the new vector.
 */
vect* vect_new(ring* r, char* s);

/**
   @brief Construct a new vector by reading from the standard input.
   @param r is the ring of the matrix elements.
   @return the new vector.
 */
vect* vect_read(ring* r);

/**
   @brief Destroy a vector.
   @param v is the vector to be destroyed.
 */
void vect_destroy(vect* v);

/**
   @brief Wrapper for vect_destroy, useful when freeing an array of vectors.
   @param p is the vector to be destroyed cast to void.
 */
void vect_destroyvoid(void* p);

/**
   @brief Initializes a vector.
   @param r is the ring of the vector elements.
   @param v is the vector to be initialized.
   @param s is the string representation of the vector.
 */
void vect_init(ring* r, vect* v, char* s);

/**
   @brief Set the elements of a vector by reading from a string.
   @param v is the vector to be set.
   @param s is the string representation of the vector.
 */
void vect_set(vect* v, char* s);

/**
   @brief Get the string representation of a vector.
   @param v is the vector.
   @param s is the string where to store the representation.
 */
char* vect_get(vect* v, char* s);

/**
   @brief Get an element of a vector.
   @param v is the vector.
   @param i is the index of the element.
   @return the element of the vector.
 */
scal* vect_getelem(vect const* v, unsigned i);

/**
   @brief Get the ring of the elements of a vector.
   @param v is the vector.
   @return the ring of the elements of the vector.
 */
ring* vect_getring(vect const* v);

/**
   @brief Arithmetic sum @f$res = op_0 + op_1 @f$ of two vectors.
   @param res is the vector storing the result.
   @param op0 is the first operand.
   @param op1 is the second operand.
 */
void vect_add(vect* res, vect* op0, vect* op1);

/**
   @brief Arithmetic difference @f$res = op_0 - op_1 @f$ of two vectors.
   @param res is the vector storing the result.
   @param op0 is the first operand.
   @param op1 is the second operand.
 */
void vect_sub(vect* res, vect* op0, vect* op1);

/**
   @brief Multiplication by a scalar and sum @f$res = res + x \mathbf{v} @f$.
   @param res is the vector storing the result.
   @param x is the multiplicative factor.
   @param v is the vector.
 */
void vect_addmul(vect* res, scal* x, vect* v);

/**
   @brief Multiplication by an integer and sum @f$res = res + i \mathbf{v} @f$.
   @param res is the vector storing the result.
   @param i is the multiplicative factor.
   @param v is the vector.
 */
void vect_addmul_i(vect* res, int i, vect* v);

/**
   @brief Multiplication by an integer @f$ res = i \mathbf{v} @f$.
   @param res is the vector storing the result.
   @param i is the multiplicative factor.
   @param v is the vector.
 */
void vect_mul_i(vect* res, int i, vect* v);

/**
   @brief Squared 2-norm of a vector @f$ res = v_0^2 + v_1^2 + v_2^2 @f$.
   @param res is the vector storing the result.
   @param v is the vector.
 */
void vect_norm2(scal* res, vect* v);

/**
   @brief Inner product @f$ res = \mathbf{v}_0 \cdot \mathbf{v} @f$.
   @param res is the vector storing the result.
   @param v0 is the first operand.
   @param v1 is the first operand.
 */
void vect_inprod(scal* res, vect const* v0, vect const* v1);

/**
   @brief Test equality of vectors.
   @param op0 is the first operand.
   @param op1 is the first operand.
   @return true if @f$ op_0 = op_1@f$, false otherwise.
 */
int vect_eq(vect const* op0, vect const* op1);

//void vect_flipvoid(void* p);

/**
   @brief Get the dimension of a vector (at present, hard coded to three).
   @param v is the vector.
   @return the dimension.
 */
unsigned vect_getdim(vect* v);

#endif
