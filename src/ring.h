/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.

   @section DESCRIPTION
   
   This  header file  provides  the interface of a ring.
*/

#ifndef RING
#define RING

#include "scal.h"

typedef struct scal scal;

typedef struct ring ring; 

/**
   @struct ring
   @brief Public functions and private parameters of ring.
*/
typedef struct ring {
    void (*init)(scal* x, ring* r); /**< @brief Ring initializing function */
    void (*free)(scal* x); /**< @brief Ring freeing function */

    void (*set)(scal* x, char* s); /**< @brief Set an element of the ring */
    char* (*get)(scal const* x, char* s); /**< @brief Get an element of the ring */

    void (*copy)(scal* dest, scal const* src); /**< @brief Copy an element of the ring */
  
    void (*add)(scal* res, scal const* op0, scal const* op1); /**< @brief Add two elements of the ring */
    void (*sub)(scal* res, scal const* op0, scal const* op1); /**< @brief Subtract two elements of the ring */
    void (*mul)(scal* res, scal const* op0, scal const* op1);  /**< @brief Multiplies two elements of the ring */
    void (*addmul)(scal* res, scal const* op0, scal const* op1);  /**< @brief Multiplies and add two elements of the ring */
    void (*addmul_i)(scal* res, int op0, scal const* op1);  /**< @brief Multiplies and add an integer and an element of the ring */
    void (*mul_i)(scal* res, int op0, scal const* op1); /**< @brief Multiplies an integer and an element of the ring */
  
    int (*comp)(scal const* op0, scal const* op1); /**< @brief Compares two elements of the ring */

    void* v; /**< @brief Private parameters of the ring. */
} ring;

typedef void ring_init_t(ring* r, char* s);
typedef void ring_free_t(ring* r);

/**
   @brief Construct a new ring.
   @param s is a string of parameters.
   @param init is the ring initializing function.
   @return the new ring.
 */
ring* ring_new(char* s, ring_init_t init);

/**
   @brief Destroy a ring.
   @param r is the ring to be destroyed.
   @param free is the ring freeing function.
 */
void ring_destroy(ring* r, ring_free_t free);

#endif
