/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.

   @section DESCRIPTION
   
   This  header file  provides  the  interface to  functions
   manipulating nodes of a doubly-linked list.
*/

#ifndef NODE
#define NODE

typedef struct node node;

/**
   @brief Construct a new node.
   @param d is the data to be stored by the node.
   @return the new node.
 */
node* node_new(void* d);

/**
   @brief Destroy a node.
   @param n is the node to be destroyed.
 */
void node_destroy(node* n);

/**
   @brief Initialize a node.
   @param n is the node to be initialized.
   @param d is the data to be stored in the node.
 */
void node_init(node* n, void* d);

/**
   @brief Free a node.
   @param n is the node to be freed.
 */
void node_free(node* n);

/**
   @brief Get the data stored in a node.
   @param n is the node.
   @return the stored data.
 */
void* node_get(node *n);

/**
   @brief Set the data stored in a node.
   @param n is the node.
   @param d is the data to be stored.
 */
void node_set(node *n, void* d);

/**
   @brief Concatenates two nodes
   @param n0 is the node to preceed n1.
   @param n1 is the node to follow n0.
 */
void node_cat(node *n0, node* n1);

/**
   @brief Get the previous node.
   @param n is the node.
   @return the node that preceeds n.
 */
node* node_prev(node* n);

/**
   @brief Get the next node.
   @param n is the node.
   @return the node that follows n.
 */
node* node_next(node* n);

#endif
