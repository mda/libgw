/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.
*/

#include <gmp.h>
#include <stdlib.h>
#include <stdio.h>

#include "../ring.h"
#include "../scal.h"

static void intgmp_scal_init(scal* op, ring* r){
    op->r = r;
    op->v = calloc(1, sizeof(mpz_t));
    mpz_t *x = op->v;
    mpz_init(x[0]);
}

static void intgmp_scal_free(scal* op) {
    mpz_t *x = op->v;
    mpz_clear(x[0]);
    free(op->v);
}

static void intgmp_scal_set(scal* op, char* s) {
    mpz_t *x = op->v;

    if (s) mpz_set_str(x[0], s, 10);
    else mpz_set_si(x[0], 0L);
}

static char* intgmp_scal_get(scal const* op, char* s) {
    mpz_t *x = op->v;

    return mpz_get_str(s, 10, x[0]);
}

static void intgmp_scal_copy(scal* dest, scal const* src) {
    mpz_t *x = dest->v, *y = src->v;
    mpz_set(x[0], y[0]);
}

static void intgmp_scal_add(scal* res, scal const* op0, scal const* op1) {
    mpz_t *x = res->v, *y = op0->v, *z = op1->v; 
    mpz_add(x[0], y[0], z[0]);
}

static void intgmp_scal_sub(scal* res, scal const* op0, scal const* op1) {
    mpz_t *x = res->v, *y = op0->v, *z = op1->v; 
    mpz_sub(x[0], y[0], z[0]);
}

static void intgmp_scal_mul(scal* res, scal const* op0, scal const* op1) {
    mpz_t *x = res->v, *y = op0->v, *z = op1->v;

    mpz_mul(x[0], y[0], z[0]);
}

static void intgmp_scal_addmul(scal* res, scal const* op0, scal const* op1) {
    mpz_t *x = res->v, *y = op0->v, *z = op1->v;

    mpz_addmul(x[0], y[0], z[0]);    
}

static void intgmp_scal_mul_i(scal* res, int i, scal const* op1) {
    mpz_t *x = res->v, *z = op1->v;
    mpz_mul_si(x[0], z[0], i);
}

static void intgmp_scal_addmul_i(scal* res, int i, scal const* op1) {
    mpz_t *x = res->v, *z = op1->v;
    
    mpz_t tmp;
    mpz_init(tmp);
    mpz_mul_si(tmp, z[0], i);
    mpz_add(x[0], x[0], tmp);
    mpz_clear(tmp);
}

static int intgmp_scal_comp(scal const* op0, scal const* op1) {
    mpz_t *x = op0->v, *y = op1->v;

    return mpz_cmp(x[0], y[0]);
}

#include "common.c"
