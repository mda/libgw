/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.
*/

#include <gmp.h>
#include <stdlib.h>
#include <stdio.h>

#include "../ring.h"
#include "../scal.h"

static void goldgmp_scal_init(scal* op, ring* r){
    op->r = r;
    op->v = calloc(2, sizeof(mpz_t));
    mpz_t *x = op->v;
    for (unsigned n = 0; n < 2; ++n)
	mpz_init(x[n]);
}

static void goldgmp_scal_free(scal* op) {
    mpz_t *x = op->v;
    for (unsigned n = 0; n < 2; ++n)
	mpz_clear(x[n]);
    free(op->v);
}

static void goldgmp_scal_set(scal* op, char* s) {
    mpz_t *x = op->v;
    if (s) {
	char s0[256], s1[256], *ps = s, *ps0 = s0, *ps1 = s1;
	while (*ps != '_') *(ps0++) = *(ps++);
	*ps0 = '\0';
	++ps;
	while (*ps != '\0') *(ps1++) = *(ps++);
	*ps1 = '\0';
	mpz_set_str(x[0], s0, 10);
	mpz_set_str(x[1], s1, 10);
    } else {
	mpz_set_si(x[0], 0L);
	mpz_set_si(x[1], 0L);
    }
}

static char* goldgmp_scal_get(scal const* op, char* s) {
    mpz_t *x = op->v;
    char s0[256], s1[256];
    sprintf(s, "%s_%s", mpz_get_str(s0, 10, x[0]), mpz_get_str(s1, 10, x[1]));
    return s;
}

static void goldgmp_scal_copy(scal* dest, scal const* src) {
    mpz_t *x = dest->v, *y = src->v;
    for (unsigned n = 0; n < 2; ++n)
	mpz_set(x[n], y[n]);
}

static void goldgmp_scal_add(scal* res, scal const* op0, scal const* op1) {
    mpz_t *x = res->v, *y = op0->v, *z = op1->v; 
    for (unsigned n = 0; n < 2; ++n)
	mpz_add(x[n], y[n], z[n]);
}

static void goldgmp_scal_sub(scal* res, scal const* op0, scal const* op1) {
    mpz_t *x = res->v, *y = op0->v, *z = op1->v; 
    for (unsigned n = 0; n < 2; ++n)
	mpz_sub(x[n], y[n], z[n]);
}

static void goldgmp_scal_mul(scal* res, scal const* op0, scal const* op1) {
    mpz_t *x = res->v, *y = op0->v, *z = op1->v;
    long *k = res->r->v; 

    mpz_t t[2];
    mpz_inits(t[0], t[1], 0);
    
    mpz_mul(t[0], y[1], z[1]);
    mpz_mul_ui(t[0], t[0], k[0]);
    mpz_addmul(t[0], y[0], z[0]);
    
    mpz_mul(t[1], y[0], z[1]);
    mpz_addmul(t[1], y[1], z[0]);

    mpz_set(x[0], t[0]);
    mpz_set(x[1], t[1]);

    mpz_clears(t[0], t[1], 0);
}


static void goldgmp_scal_addmul(scal* res, scal const* op0, scal const* op1) {
    mpz_t *x = res->v, *y = op0->v, *z = op1->v;
    long *k = res->r->v; 

    mpz_t t[2];
    mpz_inits(t[0], t[1], 0);

    mpz_mul(t[0], y[1], z[1]);
    mpz_mul_ui(t[0], t[0], k[0]);
    mpz_addmul(t[0], y[0], z[0]);
    
    mpz_mul(t[1], y[0], z[1]);
    mpz_addmul(t[1], y[1], z[0]);
  
    mpz_add(x[0], x[0], t[0]);
    mpz_add(x[1], x[1], t[1]);
    
    mpz_clears(t[0], t[1], 0);
}

static void goldgmp_scal_mul_i(scal* res, int i, scal const* op1) {
    mpz_t *x = res->v, *z = op1->v;
    for (unsigned n = 0; n < 2; ++n)
	mpz_mul_si(x[n], z[n], i);
}

static void goldgmp_scal_addmul_i(scal* res, int i, scal const* op1) {
    mpz_t *x = res->v, *z = op1->v;

    mpz_t tmp;
    mpz_init(tmp);
    for (unsigned n = 0; n < 2; ++n) {
	mpz_mul_si(tmp, z[n], i);
	mpz_add(x[n], x[n], tmp);
    
    }
    mpz_clear(tmp);
}

static int goldgmp_scal_comp(scal const* op0, scal const* op1) {
    mpz_t *x = op0->v, *y = op1->v;
    long *k = op0->r->v; 

    mpz_t c;
    mpz_init(c);
    mpz_sub(c, x[0], y[0]);
    mpz_mul(c, c, c);

    mpz_t d;
    mpz_init(d);
    mpz_sub(d, x[1], y[1]);
    mpz_mul(d, d, d);
    mpz_mul_ui(d, d, k[0]);

    int alpha = mpz_cmp(x[0], y[0]);
    int beta = mpz_cmp(y[1], x[1]);
    int gamma = mpz_cmp(c, d);
    mpz_clears(c, d, NULL);

    int op0_geq_op1 = ((alpha >= 0) && (gamma >= 0)) || ((beta <= 0) && (gamma <= 0));
    int op0_leq_op1 = ((alpha <= 0) && (gamma >= 0)) || ((beta >= 0) && (gamma <= 0));

    return op0_geq_op1 - op0_leq_op1;
}

#include "common.c"
