/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>

#include "../ring.h"
#include "../scal.h"

static void golden_scal_init(scal* x, ring* r){
    x->r = r;
    x->v = calloc(2, sizeof(long));
}

static void golden_scal_free(scal* x) {
    free(x->v);
}

static void golden_scal_set(scal* op, char* s) {
    long *x = op->v;
    if (s) sscanf(s, "%ld_%ld", x, x + 1);
    else x[0] = x[1] = 0;
}

static char* golden_scal_get(scal const* op, char* s) {
    long *x = op->v;
    sprintf(s, "%ld_%ld", x[0], x[1]);
    return s;
}

static void golden_scal_copy(scal* dest, scal const* src) {
    long *x = dest->v, *y = src->v;
    for (unsigned n = 0; n < 2; ++n)
	x[n] = y[n];
}

static void golden_scal_add(scal* res, scal const* op0, scal const* op1) {
    long *x = res->v, *y = op0->v, *z = op1->v; 
    for (unsigned n = 0; n < 2; ++n)
	x[n] = y[n] + z[n];
}

static void golden_scal_sub(scal* res, scal const* op0, scal const* op1) {
    long *x = res->v, *y = op0->v, *z = op1->v; 
    for (unsigned n = 0; n < 2; ++n)
	x[n] = y[n] - z[n];
}

static void golden_scal_mul(scal* res, scal const* op0, scal const* op1) {
    long *x = res->v, *y = op0->v, *z = op1->v, *k = res->r->v; 
    long t[2];
    
    t[0] = y[0] * z[0] + k[0] * y[1] * z[1];
    t[1] = y[0] * z[1] + y[1] * z[0];

    x[0] = t[0];
    x[1] = t[1];
}

static void golden_scal_addmul(scal* res, scal const* op0, scal const* op1) {
    long *x = res->v, *y = op0->v, *z = op1->v, *k = res->r->v; 
    long t[2];

    t[0] = y[0] * z[0] + k[0] * y[1] * z[1];
    t[1] = y[0] * z[1] + y[1] * z[0];

    x[0] += t[0];
    x[1] += t[1];
}

static void golden_scal_mul_i(scal* res, int i, scal const* op1) {
    long *x = res->v, *z = op1->v;
    for (unsigned n = 0; n < 2; ++n)
	x[n] = i * z[n];
}

static void golden_scal_addmul_i(scal* res, int i, scal const* op1) {
    long *x = res->v, *z = op1->v;
    for (unsigned n = 0; n < 2; ++n)
	x[n] += i * z[n];
}

static int golden_scal_comp(scal const* op0, scal const* op1) {
    long *x = op0->v, *y = op1->v, *k = op0->r->v; 
    long long c = (x[0] - y[0]) * (x[0] - y[0]);
    long long d = k[0] * (x[1]- y[1]) * (x[1]- y[1]);

    int a = (x[0] >= y[0] && c >= d) || (y[1] <= x[1] && c <= d);
    int b = (x[0] <= y[0] && c >= d) || (y[1] >= x[1] && c <= d);
    
    return a - b;
}

#include "common.c"
