/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.
*/

/* #include <stdlib.h> */
/* #include <stdio.h> */
/* #include <assert.h> */

/* See
   https:gcc.gnu.org/onlinedocs/cpp/Argument-Prescan.html
   for an explanation why two macros are needed*/
#define F(A, B) A ## _ ## B
#define G(A, B) F(A, B)

void ring_init(ring* r, char* s) {
    r->init = G(RINGNAME, scal_init);
    r->free = G(RINGNAME, scal_free);

    r->set = G(RINGNAME, scal_set);
    r->get = G(RINGNAME, scal_get);

    r->copy = G(RINGNAME, scal_copy);

    r->add = G(RINGNAME, scal_add);
    r->sub = G(RINGNAME, scal_sub);
    r->mul = G(RINGNAME, scal_mul);
    r->mul_i = G(RINGNAME, scal_mul_i);
    r->addmul = G(RINGNAME, scal_addmul);
    r->addmul_i = G(RINGNAME, scal_addmul_i);

    r->comp = G(RINGNAME, scal_comp);

    r->v = calloc(NCOEFF, sizeof(long));
    long* k = r->v; 
    for (unsigned i = 0; i < NCOEFF; ++i)
	k[i] = strtol(s, &s, 10);
}

void ring_free(ring* r) {
    free(r->v);
}
