/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>

#include "node.h"

/**
   @struct node
   @brief Node of a doubly-linked list.
*/
typedef struct node {
    node *next; /**< @brief Next node. */
    node *prev; /**< @brief Previous node. */
    void* data;  /**< @brief Stored data. */
} node;

node* node_new(void* d) {
    node* n =  malloc(sizeof(node)); // TODO
    node_init(n, d);
    return n;
}

void node_destroy(node* n) {
    free(n);
}

void node_init(node* n, void* d) {
    node_set(n, d);
}

void node_free(node* n) {
    // Intentionally empty
}

void* node_get(node *n) {
    return n->data;
}

void node_set(node *n, void* d) {
    n->data = d;
}

void node_cat(node *n0, node *n1) {
    if (n0) n0->next = n1;
    n1->prev = n0;
    n1->next = 0;
}

node* node_prev(node* n) {
    return n->prev;
}

node* node_next(node* n) {
    return n->next;
}
