/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.

   @section DESCRIPTION
   
   This  header file  provides  the  interface to  functions
   manipulating matrices over an arbitrary given ring.
*/

#ifndef MATRIX
#define MATRIX

#include <stdbool.h>

#include "array.h"
#include "ring.h"
#include "scal.h"

typedef struct matrix matrix;

/**
   @brief Construct a new matrix.
   @param r is the ring of the matrix elements.
   @param nr is the number of rows.
   @param nc is the number of columns.
   @return the new matrix.
 */
matrix* matrix_new(ring* r, unsigned nr, unsigned nc);

/**
   @brief Destroy a matrix.
   @param m is the matrix to be destroyed.
 */
void matrix_destroy(matrix* m);

/**
   @brief Initializes a matrix.
   @param m is the matrix to be initialized.
   @param r is the ring of the matrix elements.
   @param nr is the number of rows.
   @param nc is the number of columns.
 */
void matrix_init(matrix* m, ring* r, unsigned nr, unsigned nc);

/**
   @brief Free a matrix.
   @param m is the matrix to be freed.
 */
void matrix_free(matrix* m);

/**
   @brief Matrix multiplication @f$ res = m_0 m_1 @f$.
   @param res is the result of the multiplication.
   @param m0 is the first operand.
   @param m1 is the second operand.
 */
void matrix_mul(matrix* res, matrix* m0, matrix* m1);

/**
   @brief Compute the Gram matrix of a tuple of vectors.
   @param m is the resulting Gram matrix.
   @param a is the tuple of vectors.
 */
void matrix_gram(matrix* m, array* a);

/**
   @brief Compute the determinant of a matrix.
   @param d is the resulting determinant.
   @param m is the matrix.
 */
void matrix_det(scal* d, matrix* m);

/**
   @brief Compare two matrices elementwise.
   @param m0 is the first operand.
   @param m1 is the second operand.
   @return true if @f$ m_0 = m_1 @f$, false otherwise.
 */
bool matrix_eq(matrix* m0, matrix* m1);

/**
   @brief Generates a string representation for a matrix.
   @param m is the matrix.
   @param s is the string where to store the string representation.
   @return s.
 */
char* matrix_tostr(matrix* m, char* s);

/**
   @brief Copy a matrix.
   @param dst is the matrix to be copied to.
   @param src is the matrix to be copied from.
 */
void matrix_cpy(matrix* dst, matrix const* src);

/**
   @brief Swap two mmatrices.
   @param m0 is the first operand.
   @param m1 is the second operand.
 */
void matrix_swap(matrix* m0, matrix* m1);

/**
   @brief Get the ring of the elements of a matrix.
   @param m is the matrix.
   @return the ring of the elements of m.
 */
ring* matrix_getring(matrix* m);

/**
   @brief Get an element of a matrix.
   @param m is the matrix.
   @param i is the row of the element to be retireved.
   @param j is the column of the element to be retireved.
   @return the element @f$(i, j)@f$ of matrix m.
 */
scal* matrix_get(matrix const* m, unsigned i, unsigned j);

/**
   @brief Set an element of a matrix.
   @param m is the matrix.
   @param i is the row of the element to be retireved.
   @param j is the column of the element to be retireved.
   @param x is the new value for element @f$(i, j)@f$.
 */
void matrix_set(matrix* m, unsigned i, unsigned j, scal* x);

/**
   @brief Get the number of rows of a matrix.
   @param m is the matrix.
   @return the number of rows.
*/
unsigned matrix_getnr(matrix* m);

/**
   @brief Get the number of columns of a matrix.
   @param m is the matrix.
   @return the number of columns.
*/
unsigned matrix_getnc(matrix* m);

#endif
