/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <stdbool.h>

#include "array.h"
#include "label.h"

/**
   @struct array
   @brief Tuple of pointer to arbitrary objects of the same type.
*/
typedef struct array {
    unsigned num; /**< Number of elements of the tuple */
    label** base; /**< Pointer to the labelled elements */
} array;

array* array_new(unsigned num) {
    array* a = malloc(sizeof(array)); // TODO
    array_init(a, num);
    return a;
}

void array_destroy(array* a, destroyvoid f) {
    array_free(a, f);
    free(a);
}

void array_init(array* a, unsigned num) {
    a->num = num;
    a->base = calloc(num, sizeof(label*)); // TODO
    for (unsigned i = 0; i < num; ++i)
	a->base[i] = label_new(i, 0); 
}

void array_free(array* a, destroyvoid* f) {
    for (unsigned i = 0; i < a->num; i++) {
        if (f) f(label_getvalue(a->base[i]));
	label_destroy(a->base[i]);
    }
    free(a->base);
}

void array_cpy(array* dst, array* src) {
    assert(array_getnum(src) == array_getnum(dst));
    for (unsigned i = 0; i < array_getnum(dst); ++i)
	label_cpy(dst->base[i], src->base[i]);
}

void array_sub(array* a, unsigned first, unsigned len) {
    assert(first + len <= array_getnum(a));
    a->base += first;
    a->num = len;
}

unsigned array_getnum(array* a) {
  return a->num;
}

void* array_get(array* a, unsigned i) {
    return label_getvalue(a->base[i]);
}

void array_set(array* a, unsigned i, void* p) {
    label_setvalue(a->base[i], p);
}

unsigned array_getid(array* a, unsigned i) {
    return label_getid(a->base[i]);
}

indexes array_swap(array* a, indexes i) {
    assert((i.j <= array_getnum(a)) && (i.k <= array_getnum(a)));
    void* t = a->base[i.j];
    a->base[i.j] = a->base[i.k];
    a->base[i.k] = t;
    return i;
}

static void idx_sort(unsigned* idx, indexes i) {
    unsigned tmp = idx[i.k];
    idx[i.k] = idx[i.j];
    idx[i.j] = tmp;
}

static void array_qsort(array* a, int left, int right, compar* cmp, void* p) {
    /**
       Based  on  quick  sort.   See  B.  W.  Kernighan  and
       D. M. Ritchie, The C programming language, chapter 5,
       page 120.
    */
    if (left >= right) return;
    array_swap(a, (indexes) {left, (left + right) / 2});
    int last = left;
    for (int i = left + 1; i <= right; i++)
	if (cmp(array_get(a, i), array_get(a, left), p) < 0)
	    array_swap(a, (indexes) {++last, i});
    array_swap(a, (indexes) {left, last});
    array_qsort(a, left, last - 1, cmp, p);
    array_qsort(a, last + 1, right, cmp, p);
}

void array_sort(array* a, compar* cmp, void* par) {
    array_qsort(a, 0, array_getnum(a) - 1, cmp,  par);
}

void array_perm(array* a, array_perm_proc f, void* p) {
    /**
       Based on Johnson-Trotter algorithm. See R. Sedgewick,
       Permutation Generation Methods,  Computing Surveys 9,
       137  (1977) and  D.  E. Knuth,  The  Art of  Computer
       Programming, Volume 4A, Section 7.2.1.2 (2011).
    */
    unsigned n = array_getnum(a);
    unsigned c[n];
    bool d[n];

    for (unsigned i = 0; i < n; ++i)
	c[i] = d[i] = 0;
    
    unsigned i = 0, x = 0;
    do {
	if (c[i] < n - 1 - i) {
	    unsigned k = !d[i] ? c[i] + x: n - 2 - i - c[i] + x;
	    c[i]++;
	    f(a, array_swap(a, (indexes) {k, k + 1}), p);
	    i = x = 0;
	} else {
	    if (!(d[i] = !d[i])) ++x;
	    c[i++] = 0;
	}
    } while (i < n);
    f(a, array_swap(a, (indexes) {n-2, n-1}), p);
}

void array_comb(array* an, unsigned t, array_comb_proc proc, void* par) {
    /**
       Based on  a Chase's algorithm.  See D. E.  Knuth, The
       Art  of  Computer  Programming,  Volume  4A,  Section
       7.2.1.3 (2011).
    */
    unsigned n = array_getnum(an);
    assert(n >= t);

    array* at = array_new(t);
    
    unsigned s = n - t, r = (s > 0) ? s : t;
    int a[n], w[n + 1], c[n];
    for (unsigned j = 0, l = 0; j < n; ++j) {
	a[j] = (j < s) ? 0 : 1;
	w[j] = 1;
	c[j] = (a[j] == 1) ? l : -1;
	if (a[j] == 1) array_set(at, (c[j] = l++), array_get(an, j));
    }
    w[n] = 1;

    unsigned j = n-1, k = n-1;
    indexes idx;
    while (1) {
    	proc(an, at, (indexes) {c[j], k}, par);

	j = r;
	while (w[j] == 0) w[j++] = 1;
	if (j == n) return;
	w[j] = 0;
	
	if ((a[j] != 0) && !(j % 2) && (a[j - 2] == 0)) { // C5 right two
	    a[j] = !(a[k = j - 2] = a[j]);
	    if (r == j) r = (k > 1) ? k : 1;
	    else if (r == k) r = j - 1;
	} else if ((a[j] == 0) && (j % 2) && (a[j - 1] == 0)) { // C7 left two
	    a[j] = !(a[k = j - 2] = a[j]);
	    if (r == k) r = j;
	    else if (r == j - 1) r = k;
	} else { // C4 and C6
	    a[j] = !(a[k = j - 1] = a[j]);
	    if ((r == j) && (j > 1)) r = k;
	    else if (r == k) r = j;
	}

	c[(a[k]) ? k : j] = c[(a[k]) ? j : k];
	array_set(at, c[(a[k]) ? k : j], array_get(an, (a[k]) ? k : j));
    }

    array_destroy(at, 0);
}

void array_reset(array* a) {
    for (unsigned i = 0; i < array_getnum(a); ++i)
	label_setid(a->base[i], i);
}

void array_tuple(array* a, array_tuple_proc visit, void* p) {
  unsigned n = array_getnum(a);
  unsigned f[n + 1];
  f[0] = 1;
  for (unsigned j = 1; j <= n; ++j)
    f[j] = j;

  /**
     Based on Ehrlich's algorithm. See D. Knuth, "The Art of
     Computer Programming", Volume 4A, Section 7.2.1.1, page
     290, Algorithm L.
  */

  for (unsigned j = 0; j < n; j = f[0], f[0] = 0, f[j] = f[j + 1], f[j + 1] = j + 1)
    visit(a, j, p);
    
  visit(a, n-1, p);
}
