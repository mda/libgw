/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.

   @section DESCRIPTION
   
   This  header file  provides  the  interface to  functions
   manipulating scalars over an arbitrary given ring.
*/

#ifndef SCAL
#define SCAL

#include "ring.h"

typedef struct ring ring;

/**
   @struct scal
   @brief Scalar over an arbitrary given ring.
*/
typedef struct scal {
    ring* r;
    void* v;
} scal;

/**
   @brief Construct a new scalar.
   @param r is the ring of the scalar.
   @return the new scalar.
 */
scal* scal_new(ring* r);

/**
   @brief Destroy a scalar.
   @param x is the scalar to be destroyed.
 */
void scal_destroy(scal* x);

/**
   @brief Initialize a scalar.
   @param x is the scalar to be initialized.
   @param r is the ring of the scalar.
 */
void scal_init(scal* x, ring* r);

/**
   @brief Free a scalar.
   @param x is the scalar to be freed.
 */
void scal_free(scal* x);

/**
   @brief Set a scalar to a given value.
   @param x is the scalar to be set.
   @param s is the string with the value.
 */
void scal_set(scal* x, char* s);

/**
   @brief Get the value of a scalar.
   @param x is the scalar.
   @param s is the string where to store the value.
 */
char* scal_get(scal const* x, char* s);

/**
   @brief Copy the value of a scalar to another scalar.
   @param dest is the scalar to be assigned the value.
   @param src is the scalar to be read from.
 */
void scal_copy(scal* dest, scal const* src);

/**
   @brief Swap the values of two scalars.
   @param op0 is the first operand.
   @param op1 is the second operand.
 */
void scal_swap(scal* op0, scal* op1);

/**
   @brief Add two scalars @f$ res = op_0 + op_1@f$.
   @param res is the scalar storing the result.
   @param op0 is the first operand.
   @param op1 is the second operand.
 */
void scal_add(scal* res, scal const* op0, scal const* op1);

/**
   @brief Subtract two scalars @f$ res = op_0 - op_1@f$.
   @param res is the scalar storing the result.
   @param op0 is the first operand.
   @param op1 is the second operand.
 */
void scal_sub(scal* res, scal const* op0, scal const* op1);

/**
   @brief Multiply two scalars @f$ res = op_0 \cdot op_1@f$.
   @param res is the scalar storing the result.
   @param op0 is the first operand.
   @param op1 is the second operand.
 */
void scal_mul(scal* res, scal const* op0, scal const* op1);

/**
   @brief Multiply an integer and a scalar @f$ res = op_0 \cdot op_1@f$.
   @param res is the scalar storing the result.
   @param op0 is the integer operand.
   @param op1 is the scalar operand.
 */
void scal_mul_i(scal* res, int op0, scal const* op1);

/**
   @brief Multiply and add two scalars @f$ res = res + op_0 \cdot op_1@f$.
   @param res is the scalar storing the result.
   @param op0 is the first operand.
   @param op1 is the second operand.
 */
void scal_addmul(scal* res, scal const* op0, scal const* op1);

/**
   @brief Multiply and add an integer and a scalar @f$ res = res + op_0 \cdot op_1@f$.
   @param res is the scalar storing the result.
   @param op0 is the integer operand.
   @param op1 is the scalar operand.
 */
void scal_addmul_i(scal* res, int op0, scal const* op1);

/**
   @brief Compare two scalars.
   @param op0 is the first operand.
   @param op1 is the second operand.
   @return @f$-1@f$ if @f$op_0 < op_1@f$, @f$1@f$ if @f$op_0 > op_1@f$, and @f$0@f$ otherwise .
 */
int scal_comp(scal const* op0, scal const* op1);

/**
   @brief Maximum of two scalars.
   @param op0 is the first operand.
   @param op1 is the second operand.
   @return @f$\max(op_0, op_1)@f$.
 */
void scal_max(scal* res, scal const* op0, scal const* op1);

#endif
