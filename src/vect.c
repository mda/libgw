/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>

#include "vect.h"
#include "ring.h"

#define VECT_LEN 3

/**
   @struct vect
   @brief Vector over an arbitrary given ring.
*/
typedef struct vect {
  ring* r; /**< @brief Ring of the vector elements. */
  scal* x[VECT_LEN]; /**< @brief Vector elements. */
} vect;

vect* vect_new(ring* r, char* s) {
  vect* v = malloc(sizeof(vect));
  v->r = r;
  vect_set(v, s);
  return v;
}

vect* vect_read(ring* r) {
  char line[256];
  fgets(line, 256, stdin);

  /* vect* v = vect_new(r, line); */
  /* char s[256]; */
  /* printf("%s\n", vect_get(v, s)); */
  /* return v; */
  return vect_new(r, line);
}

void vect_destroy(vect* v) {
  char s[256];
  vect_get(v, s);
  for (int i = 0; i < VECT_LEN; ++i)
    scal_destroy(v->x[i]);
  free(v);
}

void vect_destroyvoid(void* v) {
  vect_destroy((vect*) v);
}

void vect_init(ring* r, vect* v, char* s) {
  v->r = r;
  vect_set(v, s);
}

void vect_set(vect* v, char* s) {
    char t[3][256];
    if (s) { 
	sscanf(s, "%s %s %s", t[0], t[1], t[2]);
	v->x[0] = scal_new(v->r);
	scal_set(v->x[0], t[0]);
	v->x[1] = scal_new(v->r);
	scal_set(v->x[1], t[1]);
	v->x[2] = scal_new(v->r);
	scal_set(v->x[2], t[2]);
    } else {
	v->x[0] = scal_new(v->r);
	v->x[1] = scal_new(v->r);
	v->x[2] = scal_new(v->r);
    }
}

char* vect_get(vect* v, char* s) {
  char t[3][256];
  sprintf(s, "%s %s %s", scal_get(v->x[0], t[0]), scal_get(v->x[1], t[1]), scal_get(v->x[2], t[2])); // TODO
  return s;
}

void vect_add(vect* res, vect* op0, vect* op1) {
  for (int i = 0; i < VECT_LEN; ++i)
    scal_add(res->x[i], op0->x[i], op1->x[i]);
}

void vect_sub(vect* res, vect* op0, vect* op1) {
  for (int i = 0; i < VECT_LEN; ++i)
    scal_sub(res->x[i], op0->x[i], op1->x[i]);
}

void vect_addmul(vect* res, scal* x, vect* v) {
  for (int i = 0; i < VECT_LEN; ++i)
    res->r->addmul(res->x[i], x, v->x[i]);
}

void vect_addmul_i(vect* res, int i, vect* v) {
  for (int j = 0; j < VECT_LEN; ++j)
    scal_addmul_i(res->x[j], i, v->x[j]);
}

void vect_mul_i(vect* res, int i, vect* v) {
  for (unsigned j = 0; j < VECT_LEN; ++j)
    scal_mul_i(res->x[j], i, v->x[j]);
}

void vect_norm2(scal* res, vect* v) {
    scal_set(res, 0);
    for (int i = 0; i < VECT_LEN; ++i)
	scal_addmul(res, v->x[i], v->x[i]);
}

void vect_inprod(scal* res, vect const* v0, vect const* v1) {
    scal_set(res, 0);
    for (int i = 0; i < VECT_LEN; ++i)
	scal_addmul(res, v0->x[i], v1->x[i]);
}

int vect_eq(vect const* op0, vect const* op1) {
  for (unsigned i = 0; i < VECT_LEN; ++i)
    if (scal_comp(op0->x[i], op1->x[i])) return 0; 
  return 1;
}

ring* vect_getring(vect const* v) {
    return v->r;
}

unsigned vect_getdim(vect* v) {
    return 3;
}

scal* vect_getelem(vect const* v, unsigned i) {
    return v->x[i];
}
