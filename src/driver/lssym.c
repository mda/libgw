/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>

#include "dlh.h"
#include "vect.h"
#include "array.h"
#include "ring.h"
#include "matrix.h"
#include "sym.h"

int main(int argc, char* argv[]) {
    char line[256], ringname[256], name[256], filename[256], vs[256];
    int c;
    unsigned nvec;
  
    while (1) {
	if ((c = fgetc(stdin)) == EOF) break;
	else ungetc(c, stdin);
	sscanf(fgets(line, 256, stdin), "%s", name);
	sscanf(fgets(ringname, 256, stdin), "%s", filename);
  
	void* h = dlh_new(filename);
	ring* r = ring_new(ringname + strlen(filename), (ring_init_t*) dlh_load(h, "ring_init"));

	sscanf(fgets(line, 256, stdin), "%d", &nvec);

	array* a = array_new(nvec);
	for (unsigned i = 0; i < array_getnum(a); ++i)
	  array_set(a, i, vect_read(r));
	/* if ((c = fgetc(stdin)) == EOF) break; */
	/* else ungetc(c, stdin); */
	/* sscanf(fgets(line, 256, stdin), "%s", name); */
	/* void* h = dlh_new(filename); */

	/* sscanf(fgets(line, 256, stdin), "%s", filename); */
	/* ring* r = ring_new("5", (ring_init_t*) dlh_load(h, "ring_init")); */

	/* sscanf(fgets(line, 256, stdin), "%d", &num); */
	/* array* a = array_new(num); */
	/* for (unsigned i = 0; i < array_getnum(a); ++i) */
	/*     array_set(a, i, vect_read(r)); */

	//	printf("%s\n%s\n%d\n", name, filename, num);
	ringname[strlen(ringname) - 1] = '\0';
	printf("%s\n%s\n%d\n", name, ringname, nvec);


	sym* s = sym_new(a);

	sym_proc(s, a);

	for (unsigned i = 0; i < array_getnum(sym_getarray(s)); ++i)
	    printf("%s\n", vect_get(array_get(sym_getarray(s), i), vs));

	printf("%d\n", sym_getnum(s));
	
	for (node* nd = sym_getnode(s); nd != 0; nd = node_prev(nd)) {
	    array* as = node_get(nd);
	    unsigned n = array_getnum(as);

	    for (unsigned i = 0; i < n; ++i)
		printf("%2d%c", array_getid(as, i), (i != n - 1) ? ' ' : '\n');

	    array_destroy(as, 0);
	    node_destroy(nd);
	}

	sym_destroy(s);
	array_destroy(a, vect_destroyvoid);
	// r->selfdestruct(r);
	dlh_destroy(h);
    }
}
