/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <dlfcn.h>

#include "dlh.h"

dlh dlh_new(char* filename) {
  void* h = dlopen(filename, RTLD_LAZY);
  if (!h) fprintf(stderr, "%s\n", dlerror());
  dlerror();
  return h;
}

void dlh_destroy(dlh h) {
  dlclose(h);
}

func* dlh_load(dlh h, char* funcname) {
  char* error;
  func* f;
  *(void**) (&f) = dlsym(h, funcname); // Undefined behaviour, but recommended by POSIX
  error = dlerror();
  if (error) fprintf(stderr, "%s\n", error);
  return f;
}
