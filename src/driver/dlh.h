/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.

   @section DESCRIPTION
   
   This  header file  provides  the  interface to  functions
   for dynamic loading of shared libraries.
*/

#ifndef DLH
#define DLH

typedef void* dlh;

typedef void func(void); 

/**
   @brief Construct a new dynamic loader handle.
   @param filename is the file name of the shared library.
   @return the new handle.
 */
dlh dlh_new(char* filename);

/**
   @brief Destroy a dynamic loader handle.
   @param h is the handle to be destroyed.
 */
void dlh_destroy(dlh h);

/**
   @brief Load a function from a dynamic loader handle.
   @param h is the handle.
   @param funcname is the function name to be loaded.
 */
func* dlh_load(dlh h, char* funcname);

#endif
