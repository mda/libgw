/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "matrix.h"

#include "vect.h"

/**
   @struct matrix
   @brief Matrix over an arbitrary given ring.
*/
typedef struct matrix {
    ring* r; /**< @brief Ring of the matrix elements */
    unsigned nr; /**< @brief Number of rows */
    unsigned nc; /**< @brief Number of columns */
    scal** elem; /**< @brief Matrix elements */
} matrix;

matrix* matrix_new(ring* r, unsigned nr, unsigned nc) {
    matrix* m = malloc(sizeof(matrix)); // TODO
    matrix_init(m, r, nr, nc);
    return m;
}
void matrix_destroy(matrix* m) {
    matrix_free(m);
    free(m);
}

void matrix_init(matrix* m, ring* r, unsigned nr, unsigned nc) {
    m->r = r;
    m->nr = nr;
    m->nc = nc;
    m->elem = calloc(nr * nc, sizeof(scal*)); // TODO

    for (unsigned i = 0; i < nr; ++i)
	for (unsigned j = 0; j < nr; ++j)
	    matrix_set(m, i, j, scal_new(r));
}

void matrix_free(matrix* m) {
    for (unsigned i = 0; i < m->nr; ++i)
	for (unsigned j = 0; j < m->nr; ++j)
	    scal_destroy(matrix_get(m, i, j));
    free(m->elem);
}

scal* matrix_get(matrix const* m, unsigned i, unsigned j) {
    return m->elem[i * m->nc + j];
}

void matrix_set(matrix* m, unsigned i, unsigned j, scal* x) {
    m->elem[i * m->nc + j] = x;
}

void matrix_gram(matrix* m, array* a) {
    for (unsigned i = 0; i < array_getnum(a); ++i)
	for (unsigned j = 0; j < array_getnum(a); ++j)
	    vect_inprod(matrix_get(m, i, j), array_get(a, i), array_get(a, j));
}

static void matrix_bird(matrix* res, matrix* m) {
    for (unsigned i = 0; i < m->nr; ++i)
	for (unsigned j = i + 1; j < m->nr; ++j)
	    scal_copy(matrix_get(res, i, j), matrix_get(m, i, j));

    for (unsigned i = 0; i < m->nr - 1; ++i) {
	scal_set(matrix_get(res, i, i), 0);
	for (unsigned k = i + 1; k < m->nr; ++k)
	    scal_sub(matrix_get(res, i, i), matrix_get(res, i, i), matrix_get(m, k, k));
    }
    scal_set(matrix_get(res, res->nr - 1, res->nc - 1), 0); // TODO
    
    for (unsigned i = 0; i < m->nr; ++i)
	for (unsigned j = 0; j < i; ++j)
	    scal_set(matrix_get(res, i, j), 0); // TODO
}

void matrix_det(scal* d, matrix* m) {
    assert((m->nr == m->nc));

    /**
       Based on  Bird's division-free algorithm. See  R.  S.
       Bird, A simple  division-free algorithm for computing
       determinants,  Information  Processing  Letters  111,
       1072 (2011).
    */
    matrix* tmp = matrix_new(m->r, m->nr, m->nc);
    matrix_cpy(tmp, m);

    for (unsigned i = 0; i < m->nr - 1; ++i) {
	matrix_bird(tmp, tmp);
	matrix_mul(tmp, tmp, m);
    }
    scal_copy(d, matrix_get(tmp, 0, 0));
    if (!(m->nr % 2)) scal_mul_i(d, -1, d);
    matrix_destroy(tmp);
}

bool matrix_eq(matrix* m0, matrix* m1) {
  assert((m0->r == m1->r) && (m0->nc == m1->nc) && (m0->nr == m1->nr));

  for (unsigned i = 0; i < m0->nr; ++i)
    for (unsigned j = 0; j < m0->nc; ++j)
	if (scal_comp(matrix_get(m0, i, j), matrix_get(m1, i, j))) return false;

  return true;
}

void matrix_mul(matrix* res, matrix* m0, matrix* m1) {
    assert((m0->r == m1->r) && (m0->nc == m1->nr));
    matrix* tmp = matrix_new(res->r, res->nr, res->nc);

    /* This is naive matrix multiplication. */
    for (unsigned i = 0; i < m0->nr; ++i)
	for (unsigned j = 0; j < m1->nc; ++j) {
	    scal_mul(matrix_get(tmp, i, j), matrix_get(m0, i, 0), matrix_get(m1, 0, j));
	    for (unsigned k = 1; k < m0->nc; ++k)
		scal_addmul(matrix_get(tmp, i, j), matrix_get(m0, i, k), matrix_get(m1, k, j));
	}
    
    matrix_swap(tmp, res);
    matrix_destroy(tmp);
}

char* matrix_tostr(matrix* m, char* s) {
    char t[256];

    s[0] = '\0';
    for (unsigned i = 0; i < m->nr; ++i){
	for (unsigned j = 0; j < m->nc; ++j) {
	    strcat(s, scal_get(matrix_get(m, i, j), t));
	    strcat(s, "\t");
	}
	strcat(s, "\n");
    }

    return s;
}

void matrix_cpy(matrix* dst, matrix const* src) {
    assert(dst->r == src->r && dst->nr == src->nr && dst->nc == src->nc);

    for (unsigned i = 0; i < dst->nr; ++i)
      for (unsigned j = 0; j < dst->nc; ++j)
	  scal_copy(matrix_get(dst, i, j), matrix_get(src, i, j));
}

void matrix_swap(matrix* m0, matrix* m1) {
    assert(m0->r == m1->r && m0->nr == m1->nr && m0->nc == m1->nc);

    scal** tmp = m0->elem;
    m0->elem = m1->elem;
    m1->elem = tmp;    
}

ring* matrix_getring(matrix* m) {
    return m->r;
}

unsigned matrix_getnr(matrix* m) {
    return m->nr;
}

unsigned matrix_getnc(matrix* m) {
    return m->nc;
}
