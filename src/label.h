/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.

   @section DESCRIPTION
   
   This  header file  provides  the  interface to  functions
   manipulating the labels of each elements of any tuple.
*/

#ifndef LABEL
#define LABEL

typedef struct label label;

/**
   @brief Construct a new label for a tuple element.
   @param i is the id of the label of the tuple element.
   @param v is the value stored by the tuple element.
   @return the new label.
 */
label* label_new(unsigned i, void* v);

/**
   @brief Destroy a label for a tuple element.
   @param l is the label to be destroyed.
 */
void label_destroy(label* l);

/**
   @brief Get the id of a label for a tuple element.
   @param l is the label.
   @return the id of the label.
 */
unsigned label_getid(label const* l);

/**
   @brief Set the id of a label for a tuple element.
   @param l is the label.
   @param i is the id to be given to the label.
 */
void label_setid(label* l, unsigned i);

/**
   @brief Get the value of a label for a tuple element.
   @param l is the label.
   @return the value of the label.
 */
void* label_getvalue(label const* l);

/**
   @brief Set the value of a label for a tuple element.
   @param l is the label.
   @param i is the value to be given to the label.
 */
void label_setvalue(label* l, void* v);

/**
   @brief Copy a label to another label.
   @param dst is the label to be copied to.
   @param src is the label to be copied from.
 */
void label_cpy(label* dst, label* src);

#endif
