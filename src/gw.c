/**
   @file
   @author Michele Dall'Arno <michele dot dallarno at protonmail dot com>

   @copyright Copyright 2021 Michele Dall'Arno

   @section LICENSE

   This file is part of libgw.

   libgw is  free software:  you can redistribute  it and/or
   modify  it under  the  terms of  the  GNU General  Public
   License  as published  by the  Free Software  Foundation,
   either version 3 of the  License, or (at your option) any
   later version.

   libgw is distributed in the  hope that it will be useful,
   but  WITHOUT  ANY  WARRANTY;  without  even  the  implied
   warranty of  MERCHANTABILITY or FITNESS FOR  A PARTICULAR
   PURPOSE.   See the  GNU General  Public License  for more
   details.

   You should have received a copy of the GNU General Public
   License    along    with     libgw.     If    not,    see
   <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include "gw.h"
#include "vect.h"

typedef void gw_branch(gw* g);

/**
   \struct gw
   \brief State of the guesswork-computing algorithm.
*/
typedef struct gw {
    array* a; /**< Tuple of Pauli vectors  */
    unsigned n; /**< Cardinality of the ensemble */
    unsigned k; /**< */ 
    gw_branch* f; /**< */
    vect* v; /**< Average Pauli vector */
    scal* x; /**< Maximum squared norm */
} gw;

static void gw_cs(gw* g, sym* s); // tests central symmetry
static void gw_vt(gw* g, sym* s); // tests vertex transitivity

static void gw_compute(gw* g);

static void gw_stage0(array* a, indexes i, void *p); // perms
static void gw_stage1(array* a, unsigned i, void* p); // tuple

static void gw_branch0(gw* g);
static void gw_branch1(gw* g);

gw* gw_new(sym* s) {
    gw* g = malloc(sizeof(gw));
    gw_init(g, s);
    return g;
}

void gw_destroy(gw* g) {
    gw_free(g);
    free(g);
}

void gw_init(gw* g, sym* s) {
    g->a = sym_getarray(s);
    g->k = 1;
    g->n = array_getnum(g->a);

    ring* r = vect_getring(array_get(g->a, 0));
    g->x = scal_new(r);
    g->v = vect_new(r, 0);
    g->f = gw_branch0;
    gw_compute(g);   

    gw_cs(g, s);
    gw_vt(g, s);
}

void gw_free(gw* g) {
    scal_destroy(g->x);
    vect_destroy(g->v);
}

static void gw_cs(gw* g, sym* s) {
    vect* tmp = vect_new(vect_getring(g->v), 0);
    for (unsigned i = 0; i < array_getnum(g->a); ++i) {
	vect_mul_i(tmp, -1, array_get(g->a, i));
	if (!vect_eq(tmp, array_get(g->a, array_getnum(g->a) - 1 - i))) {
	    fprintf(stderr, "Central symmetry: no\n");
	    return;
	}
    }
    vect_destroy(tmp);

    fprintf(stderr, "Central symmetry: yes\n");
    array_sub(g->a, 0, array_getnum(g->a) / 2);
    g->k = 2;
    g->f = gw_branch1;
}

static void gw_vt(gw* g, sym* s) {
    bool b[g->n]; // Use s instead of g->n

    for (unsigned i = 0; i < g->n; ++i)
	b[i] = false;
    
    for (node* n = sym_getnode(s); n != 0; n = node_prev(n))
	b[array_getid(node_get(n), 0)] = true;

    for (unsigned i = 0; i < array_getnum(g->a); ++i)
	if (b[i] == false) {
	    fprintf(stderr, "Vertex transitivity: no\n");
	    return;
	}
    
    fprintf(stderr, "Vertex transitivity: yes\n");
    array_sub(g->a, 0, array_getnum(g->a) - 1);
}

static unsigned gw_coeff(gw* g, unsigned i) {
    return 2 * i - g->n + 1;
}

static void gw_compute(gw* g) {
    for (int i = 0; i < array_getnum(g->a); ++i)
	vect_addmul_i(g->v, gw_coeff(g, i), (vect*) array_get(g->a, i));
    vect_norm2(g->x, g->v);
}

static void gw_stage0(array* a, indexes i, void *p) { // perms
    gw* g = p;
    
    int d = 2 * g->k * (i.j - i.k);
    vect_addmul_i(g->v, d, (vect*) array_get(a, i.j));
    vect_addmul_i(g->v, -d, (vect*) array_get(a, i.k));

    g->f(g);
}

static void gw_stage1(array* a, unsigned i, void* p) { // tuple
    gw* g = p;

    vect_mul_i(array_get(a, i), -1, array_get(a, i));
    vect_addmul_i(g->v, 4 * gw_coeff(g, i), array_get(a, i));
    
    gw_branch0(g);
}

static void gw_branch0(gw* g) {
    ring* r = vect_getring(g->v);
    static unsigned c = 0;
    scal* x = scal_new(vect_getring(g->v));
    vect_norm2(x, g->v);
    scal_max(g->x, g->x, x);
    scal_destroy(x);
}

static void gw_branch1(gw* g) {
    array_tuple(g->a, gw_stage1, g);
}

void gw_proc(gw* g) {
    array_perm(g->a, gw_stage0, g);
}

scal* gw_get(gw* g) {
    return g->x;
}
