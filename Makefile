CC = clang -O3 -std=c17 -pedantic-errors

# all: integer.so intgmp.so golden.so goldgmp.so
all: src

clean:
	trash *~; \
	$(MAKE) clean -C src

.PHONY: src

src:
	$(MAKE) -C src
