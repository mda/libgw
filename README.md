# libgw

A solver for the  quantum-information theoretical problem of
the minimum guesswork of a quantum ensemble.

The  guesswork  quantifies  the minimum  number  of  queries
needed to guess  the state of an ensemble, when  one is only
allowed to query  one state at a time.  `libgw` computes the
exact analytical closed-form expression of the guesswork for
any given ensemble of  qubit states with uniform probability
distribution.

`libgw`  was  written  by  Michele  Dall'Arno  (michele  dot
dallarno at protonmail dot com)  and is entirely free (as in
freedom) software.

The algorithms  implemented in `libgw` are  discussed in the
following works:

- M.  Dall'Arno, F.  Buscemi,  T. Koshiba,  *Guesswork of  a
  quantum ensemble*, arXiv:2012.09350,

- M.   Dall'Arno,  F.   Buscemi,   T.  Koshiba,   *Classical
  computation of quantum guesswork*, arXiv:2112.01666.

If you use `libgw` for a publication, please consider citing
these works.

---

## Building

The  main library  is  entirely written  in  C17 and  should
compile on any compliant implementation, such as a GNU+Linux
system   with    a   C    compiler   such   as    `gcc`   or
`clang`.  Additionally,  the  drivers (`lssym`  and  `lsgw`)
depend  on the  POSIX `dl`  library,  and some  of the  ring
implementations depend  on the GNU `gmp`  library. The build
system uses GNU Make.

To build, just type

    make

in the root directory of the project.

To  build the  documentation,  you will  need the  `doxygen`
documentation system. Just type

    doxygen

in the root  directory of the project.  The documentation is
generated in the /doc directory in html and Latex format.

---

## Usage

Two drivers are provided along with the main library:

- `lssym` finds the symmetries of any given input ensemble

- `lsgw`   computes   the   exact   analytical   closed-form
  expression of the guesswork of any given qubit ensemble

They can be invoked as follows:

    cat input_ensemble_file.txt | lssym

    cat input_ensemble_file.txt | lsgw

For the format of the input and output, you can refer to the
example  files in  the directory  `/data`. Notice  that each
input  ensemble  comes  with   a  shared  object  file  that
implements  the  corresponding  ring  (mainly  input/output,
addition,  multiplication,  comparison).   If  you  want  to
provide your own ring implementation, you can find a list of
the functions  to implement in the  documentation for struct
ring defined in file `ring.h`.

If you  want to use `libgw`  as a library, you  can refer to
the  documentation as  well as  to  the source  code of  the
drivers `lssym` and `lsgw` for examples.
